#include <iostream>
#include <cmath>

class Vector{
private:
    double x=0, y=0, z=0;
public:
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void getCoords(){
        std::cout << "\nx=" << x << " y=" << y << " z=" << z;
    }
    double getLength(){
        return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
    }
};

int main() {
    Vector v(4,0,3);
    v.getCoords();
    std::cout << "\n" << v.getLength();
    return 0;
}